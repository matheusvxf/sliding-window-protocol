#! /usr/bin/perl -w

use strict;

my $file;
my $i;

open($file, ">", "input_10.in");

for (my $i=0; $i <= 1000; $i++) {
   print $file "msg 0 0 Package $i \n";
}


open($file, ">", "input_hard.in");

my $senders = 5;
my $receivers = 5;
my $max_msg_size = 1024;

for(my $i = 0; $i < 5000; ++$i){
    my $sender = int_rand($senders);
    my $receiver = int_rand($receivers);
    my $msg_size = int_rand($max_msg_size);
    my $msg = "";
    
    for(my $j = 0; $j < $msg_size; ++$j){
        $msg .= chr(ord("a") + int_rand(ord("w") - ord("a")));
    }
    
    print $file "msg $sender $receiver $msg\n";
    
}

sub int_rand
{
    my $a = shift @_;
    return int(rand($a));
}
