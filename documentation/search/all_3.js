var searchData=
[
  ['data',['data',['../struct_frame.html#a113ae2169acd71e2d9739fbccbebdf1a',1,'Frame']]],
  ['debug',['DEBUG',['../common_8h.html#ad72dbcf6d0153db1b8d8a58001feed83',1,'common.h']]],
  ['debug_5fassert',['DEBUG_ASSERT',['../common_8h.html#aa583c348c858a7681f097347158598fb',1,'common.h']]],
  ['default_5finput_5fbuffer_5fsize',['DEFAULT_INPUT_BUFFER_SIZE',['../input_8h.html#a77e39bc89b233ce6a49cc282cfee0503',1,'input.h']]],
  ['drop_5fprob',['drop_prob',['../struct_sys_config__t.html#a900b945aece401be3253cae0959e04d1',1,'SysConfig_t']]],
  ['dst_5fid',['dst_id',['../struct_cmd__t.html#a4e3aaaf127f452d10009daa6c2c89d2f',1,'Cmd_t']]]
];
