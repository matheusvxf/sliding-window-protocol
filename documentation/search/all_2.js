var searchData=
[
  ['check_5fcrc',['check_CRC',['../util_8h.html#a61700cdf588b29b301d3536fb8bcbbbd',1,'check_CRC(UINT8 msg[MAX_FRAME_SIZE]):&#160;util.c'],['../util_8c.html#a61700cdf588b29b301d3536fb8bcbbbd',1,'check_CRC(UINT8 msg[MAX_FRAME_SIZE]):&#160;util.c']]],
  ['cmd',['Cmd',['../common_8h.html#a6d390c80413fbae62562f37fb4ac5f8d',1,'common.h']]],
  ['cmd_5ft',['Cmd_t',['../struct_cmd__t.html',1,'']]],
  ['common_2eh',['common.h',['../common_8h.html',1,'']]],
  ['communicate_2ec',['communicate.c',['../communicate_8c.html',1,'']]],
  ['communicate_2eh',['communicate.h',['../communicate_8h.html',1,'']]],
  ['convert_5fchar_5fto_5fframe',['convert_char_to_frame',['../util_8h.html#aa1568b5dfda3779a6ca22fa0171b69eb',1,'convert_char_to_frame(UINT8 *char_buffer):&#160;util.c'],['../util_8c.html#aa1568b5dfda3779a6ca22fa0171b69eb',1,'convert_char_to_frame(UINT8 *char_buffer):&#160;util.c']]],
  ['convert_5fframe_5fto_5fchar',['convert_frame_to_char',['../util_8h.html#a57ad1220fafa1eed71e35ef44cecd70c',1,'convert_frame_to_char(Frame *frame):&#160;util.c'],['../util_8c.html#a57ad1220fafa1eed71e35ef44cecd70c',1,'convert_frame_to_char(Frame *frame):&#160;util.c']]],
  ['corrupt_5fprob',['corrupt_prob',['../struct_sys_config__t.html#a104c82f3d58a5b11ef47684393a595ea',1,'SysConfig_t']]],
  ['corruption_5fbits',['CORRUPTION_BITS',['../common_8h.html#ae003daa956e49094bbd52c0c3c7d6c95',1,'common.h']]],
  ['crc',['crc',['../struct_frame.html#a6fc928771607f3ffaf7e2e678e5adf08',1,'Frame::crc()'],['../util_8h.html#ac9a03db0535866917353347ac7fb4789',1,'CRC(UINT8 *array, UINT32 n_bytes):&#160;util.c'],['../util_8c.html#ac9a03db0535866917353347ac7fb4789',1,'CRC(UINT8 *array, UINT32 n_bytes):&#160;util.c']]],
  ['crc_5f32',['CRC_32',['../common_8h.html#af6aee885525d838fc93ed7d35d9c2d3b',1,'common.h']]],
  ['crc_5f8',['CRC_8',['../common_8h.html#a8e52750eccb4e3b43e187d43c47ee11c',1,'common.h']]],
  ['crc_5fsize',['CRC_SIZE',['../common_8h.html#aa93e89217587fdff12bf43b5d7f54f74',1,'common.h']]],
  ['create_5fframe',['create_frame',['../util_8h.html#a05ac8af98eb44013527fde9c9635e369',1,'create_frame(tSenderID sender, tReceiverID receiver, tHeader_flag flag):&#160;util.c'],['../util_8c.html#a05ac8af98eb44013527fde9c9635e369',1,'create_frame(tSenderID sender, tReceiverID receiver, tHeader_flag flag):&#160;util.c']]]
];
