var searchData=
[
  ['init_5freceiver',['init_receiver',['../receiver_8h.html#ab92c6c37ea565def77411fdcdd5ca9f1',1,'init_receiver(Receiver *receiver, tReceiverID ID, UINT32 senders):&#160;receiver.c'],['../receiver_8c.html#ab92c6c37ea565def77411fdcdd5ca9f1',1,'init_receiver(Receiver *receiver, tReceiverID ID, UINT32 senders):&#160;receiver.c']]],
  ['init_5freceiver_5fpeer',['init_receiver_peer',['../receiver_8c.html#a58689a1f670652e4172bae1b041abfd6',1,'receiver.c']]],
  ['init_5fsender',['init_sender',['../sender_8h.html#a27ed12a9e4390aca149a03bd92740508',1,'init_sender(Sender *sender, tSenderID ID, UINT32 receivers):&#160;sender.c'],['../sender_8c.html#a27ed12a9e4390aca149a03bd92740508',1,'init_sender(Sender *sender, tSenderID ID, UINT32 receivers):&#160;sender.c']]],
  ['init_5fsender_5fpeer',['init_sender_peer',['../sender_8c.html#aabf91ae93be69daa44a8a02785b28fc8',1,'sender.c']]],
  ['input_2ec',['input.c',['../input_8c.html',1,'']]],
  ['input_2eh',['input.h',['../input_8h.html',1,'']]],
  ['input_5fcmdlist_5fhead',['input_cmdlist_head',['../struct_sender.html#a6b928d95156c9d95b678d10324df85d7',1,'Sender']]],
  ['input_5fframelist_5fhead',['input_framelist_head',['../struct_receiver.html#a637c7a3fad4c4ae8f794e2ea537e4d93',1,'Receiver::input_framelist_head()'],['../struct_sender.html#ade29d848120ff901f487e99080b2ed7d',1,'Sender::input_framelist_head()']]],
  ['int16',['INT16',['../common_8h.html#a30f500129d8c688af07726d5d34ce52d',1,'common.h']]],
  ['int32',['INT32',['../common_8h.html#a1137216524060afd426c34677fed058b',1,'common.h']]],
  ['int64',['INT64',['../common_8h.html#a7960ce0771c3e4798168c92d15e4b0c4',1,'common.h']]],
  ['int8',['INT8',['../common_8h.html#a7ebe70ceca856797319175e30bcf003d',1,'common.h']]]
];
