var searchData=
[
  ['init_5freceiver',['init_receiver',['../receiver_8h.html#ab92c6c37ea565def77411fdcdd5ca9f1',1,'init_receiver(Receiver *receiver, tReceiverID ID, UINT32 senders):&#160;receiver.c'],['../receiver_8c.html#ab92c6c37ea565def77411fdcdd5ca9f1',1,'init_receiver(Receiver *receiver, tReceiverID ID, UINT32 senders):&#160;receiver.c']]],
  ['init_5freceiver_5fpeer',['init_receiver_peer',['../receiver_8c.html#a58689a1f670652e4172bae1b041abfd6',1,'receiver.c']]],
  ['init_5fsender',['init_sender',['../sender_8h.html#a27ed12a9e4390aca149a03bd92740508',1,'init_sender(Sender *sender, tSenderID ID, UINT32 receivers):&#160;sender.c'],['../sender_8c.html#a27ed12a9e4390aca149a03bd92740508',1,'init_sender(Sender *sender, tSenderID ID, UINT32 receivers):&#160;sender.c']]],
  ['init_5fsender_5fpeer',['init_sender_peer',['../sender_8c.html#aabf91ae93be69daa44a8a02785b28fc8',1,'sender.c']]]
];
