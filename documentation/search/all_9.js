var searchData=
[
  ['main',['main',['../main_8c.html#a0ddf1224851353fc92bfbff6f499fa97',1,'main.c']]],
  ['main_2ec',['main.c',['../main_8c.html',1,'']]],
  ['max_5fcommand_5flength',['MAX_COMMAND_LENGTH',['../common_8h.html#af1abcb51a4aa27a5a5a7958c03448134',1,'common.h']]],
  ['max_5ffragments',['MAX_FRAGMENTS',['../common_8h.html#a3e3558ae64440370a8d1eca6f4447a9c',1,'common.h']]],
  ['max_5fframe_5fsize',['MAX_FRAME_SIZE',['../common_8h.html#ad15d35a0d29a9dbf9324e3859ce3b008',1,'common.h']]],
  ['max_5fseq_5fnum',['MAX_SEQ_NUM',['../common_8h.html#a2fd6ed85e0d3e86b18c3b40a2c827f46',1,'common.h']]],
  ['message',['message',['../struct_cmd__t.html#a262a97f679c84bc8d44e14835d78e6d9',1,'Cmd_t']]],
  ['msg',['msg',['../structt_receiver__peer_1_1t_rcv__queue__slot.html#a3559857962e4f0a0280e8fbf2f3e4baf',1,'tReceiver_peer::tRcv_queue_slot']]],
  ['msg_5fsize',['MSG_SIZE',['../common_8h.html#ad4d025ecf1bdbf8b244ca688df8e478d',1,'common.h']]]
];
