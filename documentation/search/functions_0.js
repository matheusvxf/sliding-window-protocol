var searchData=
[
  ['check_5fcrc',['check_CRC',['../util_8h.html#a61700cdf588b29b301d3536fb8bcbbbd',1,'check_CRC(UINT8 msg[MAX_FRAME_SIZE]):&#160;util.c'],['../util_8c.html#a61700cdf588b29b301d3536fb8bcbbbd',1,'check_CRC(UINT8 msg[MAX_FRAME_SIZE]):&#160;util.c']]],
  ['convert_5fchar_5fto_5fframe',['convert_char_to_frame',['../util_8h.html#aa1568b5dfda3779a6ca22fa0171b69eb',1,'convert_char_to_frame(UINT8 *char_buffer):&#160;util.c'],['../util_8c.html#aa1568b5dfda3779a6ca22fa0171b69eb',1,'convert_char_to_frame(UINT8 *char_buffer):&#160;util.c']]],
  ['convert_5fframe_5fto_5fchar',['convert_frame_to_char',['../util_8h.html#a57ad1220fafa1eed71e35ef44cecd70c',1,'convert_frame_to_char(Frame *frame):&#160;util.c'],['../util_8c.html#a57ad1220fafa1eed71e35ef44cecd70c',1,'convert_frame_to_char(Frame *frame):&#160;util.c']]],
  ['crc',['CRC',['../util_8h.html#ac9a03db0535866917353347ac7fb4789',1,'CRC(UINT8 *array, UINT32 n_bytes):&#160;util.c'],['../util_8c.html#ac9a03db0535866917353347ac7fb4789',1,'CRC(UINT8 *array, UINT32 n_bytes):&#160;util.c']]],
  ['create_5fframe',['create_frame',['../util_8h.html#a05ac8af98eb44013527fde9c9635e369',1,'create_frame(tSenderID sender, tReceiverID receiver, tHeader_flag flag):&#160;util.c'],['../util_8c.html#a05ac8af98eb44013527fde9c9635e369',1,'create_frame(tSenderID sender, tReceiverID receiver, tHeader_flag flag):&#160;util.c']]]
];
