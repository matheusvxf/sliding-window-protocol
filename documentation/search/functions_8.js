var searchData=
[
  ['send_5fframe',['send_frame',['../communicate_8h.html#a344f67ac07dc88fce83844a966767d6c',1,'send_frame(char *, enum SendFrame_DstType):&#160;communicate.c'],['../communicate_8c.html#a65ecb6cc1e14e4b046efcbca1435ca10',1,'send_frame(char *char_buffer, enum SendFrame_DstType dst_type):&#160;communicate.c']]],
  ['send_5fmsg_5fto_5freceivers',['send_msg_to_receivers',['../communicate_8h.html#a87ecd9b3eb96271af4dd9f0a31aa4e89',1,'send_msg_to_receivers(char *):&#160;communicate.c'],['../communicate_8c.html#ae77ad81e07e3dbbafd3dd10b92524154',1,'send_msg_to_receivers(char *char_buffer):&#160;communicate.c']]],
  ['send_5fmsg_5fto_5fsenders',['send_msg_to_senders',['../communicate_8h.html#aa7ce489c2a4cbbc51335fe23b2c05441',1,'send_msg_to_senders(char *):&#160;communicate.c'],['../communicate_8c.html#addd6b5485aaf617be70763c5070d9bbd',1,'send_msg_to_senders(char *char_buffer):&#160;communicate.c']]],
  ['sender_5fdequeue_5fframe',['sender_dequeue_frame',['../sender_8c.html#ab6b7f83937c1cdbdfd579f2537e8af83',1,'sender.c']]],
  ['sender_5fget_5fnext_5fexpiring_5ftimeval',['sender_get_next_expiring_timeval',['../sender_8c.html#a50861ebc09025d9937472e3659d3d01b',1,'sender.c']]],
  ['sender_5fpeer_5fdequeue_5fframe',['sender_peer_dequeue_frame',['../sender_8c.html#a8d27c0ac06b2ae703af7e4247e3f6d9b',1,'sender.c']]],
  ['sender_5fpeer_5fdispatch_5fframe',['sender_peer_dispatch_frame',['../sender_8c.html#aa81a54527b5736e5bc8263e6dfe3cb06',1,'sender.c']]],
  ['sender_5fpeer_5fopen_5fwindow',['sender_peer_open_window',['../sender_8c.html#a7a4e302fd0902d7446ed71bd01f64bb0',1,'sender.c']]],
  ['sender_5fpeer_5fresend_5fframe',['sender_peer_resend_frame',['../sender_8c.html#af9ea969144d7870f507adae48742591a',1,'sender.c']]],
  ['sender_5fpeer_5fset_5fframe_5ftimeout',['sender_peer_set_frame_timeout',['../sender_8c.html#a423126ab699e27cce202e47d53eb11f5',1,'sender.c']]],
  ['sender_5fpeer_5fthere_5fis_5fwindow_5favailable',['sender_peer_there_is_window_available',['../sender_8c.html#a9a4f477718585be74b4ab40159de2f0a',1,'sender.c']]],
  ['sender_5fpeer_5fwindow_5fwrap',['sender_peer_window_wrap',['../sender_8c.html#a9a85d94e0a47fb1c7957d196f4ef4cde',1,'sender.c']]],
  ['sender_5fsend_5fdata',['sender_send_data',['../sender_8c.html#aaf7a4eb98902cf05fcbe45f785e42f68',1,'sender.c']]],
  ['sender_5fsend_5ffragments',['sender_send_fragments',['../sender_8c.html#a073c288f080f32642a6baceb7350117b',1,'sender.c']]]
];
