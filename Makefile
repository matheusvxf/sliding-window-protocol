# cory and nova use gcc version 3.3.2

VPATH = components/source

TARGETNAME=tritontalk

CC = gcc
DEBUG = -g #-v

LDFLAGS = -lresolv -lnsl -lpthread -lm

OS = LINUX

CCFLAGS = -Wall $(DEBUG) -D$(OS) -I./components/include -O0 -g3

# add object file names here
OBJS = main.o util.o input.o communicate.o sender.o receiver.o

all: tritontalk

%.o : %.c
	$(CC) -c $(CCFLAGS) $<

%.o : %.cc
	$(CC) -c $(CCFLAGS) $<

$(TARGETNAME): $(OBJS)
	$(CC) -o $(TARGETNAME) $(OBJS) $(CCFLAGS) $(LDFLAGS) 

clean:
	rm -f $(TARGETNAME) core *.o *~ 

submit: clean
	rm -f project1.tgz; tar czvf project1.tgz *; turnin project1.tgz -c cs123f -p project1
