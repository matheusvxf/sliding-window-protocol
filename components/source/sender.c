#include "sender.h"

/**< @file
@brief Sender function definitions. */

/* Sender private functions */
static void init_sender_peer(tSender_peer* peer);
static tTime* sender_get_next_expiring_timeval(Sender* sender);
static void handle_incoming_acks(Sender* sender, LLnode** outgoing_frames_head_ptr);
static void handle_input_cmds(Sender* sender, LLnode** outgoing_frames_head_ptr);
static void handle_timeout_frames(Sender* sender, LLnode** outgoing_frames_head_ptr);
static void sender_dequeue_frame(Sender* sender, LLnode** outgoing_frames_head_ptr);
static void sender_peer_dequeue_frame(tSender_peer* peer, LLnode** outgoing_frames_head_ptr);
static void sender_send_data(Sender* sender, Cmd* outgoing_cmd, LLnode** outgoing_frames_head_ptr);
static void sender_send_fragments(Sender* sender, Cmd* cmd, LLnode** outgoing_frames_head_ptr, UINT32 fragments);
static BOOLEAN sender_peer_there_is_window_available(tSender_peer* peer);
static BOOLEAN sender_peer_window_wrap(tSender_peer* peer);
static void sender_peer_dispatch_frame(tSender_peer* peer, LLnode** outgoing_frames_head_ptr, Frame* frame);
static void sender_peer_resend_frame(tSender_peer* peer, UINT8 queue_idx, LLnode** outgoing_frames_head_ptr);
static void sender_peer_set_frame_timeout(tSender_peer* peer, UINT8 window_slot);
static void sender_peer_open_window(tSender_peer* peer);

/***************************************************************************//**
*
*   Initialize the sender control block
*
*   @param[in] sender 		The sender control block
*   @param[in] ID			Sender ID
*   @param[in] receivers 	The number of receivers the sender should keep connection
*
*   @return     void
*
*******************************************************************************/
void init_sender(Sender* sender, tSenderID ID, UINT32 receivers)
{
    UINT8 i;

    memset(sender, 0, sizeof(Sender));
    sender->send_id = ID;
    sender->input_cmdlist_head = NULL;
    sender->input_framelist_head = NULL;
    sender->peer = (tSender_peer*)malloc(sizeof(tSender_peer)* receivers);
    sender->num_peers = receivers;

    for (i = 0; i < receivers; ++i)
        init_sender_peer(&(sender->peer[i]));

    pthread_cond_init(&sender->buffer_cv, NULL);
    pthread_mutex_init(&sender->buffer_mutex, NULL);
}

/***************************************************************************//**
*
*   Initialize the control block of a sender connection with a receiver
*
*   @param[in] peer The peer control block
*
*   @return     void
*
*******************************************************************************/
void init_sender_peer(tSender_peer* peer)
{
	memset(peer, 0, sizeof(tSender_peer));
    peer->LFS = -1;
    peer->LFC = -1;
    peer->LAR = -1;
    peer->LPC = -1;
    peer->frame_buffer = NULL;
}

/***************************************************************************//**
*
*   Look for the next timeout that should occur for this sender   
*
*   @param[in] sender The sender control block
*
*   @return     The next timeout for this sender
*
*******************************************************************************/
tTime* sender_get_next_expiring_timeval(Sender* sender)
{
    UINT32 i, j;
    tSender_peer peer;
    tTime* min = NULL;
    for (i = 0; i < sender->num_peers; ++i)
    {
        peer = sender->peer[i];
        for (j = 0; j < SWS; ++j)
        {
            if (peer.queue[j].frame != NULL && peer.queue[j].state == tFrame_state_ack_waiting)
            {
                if (min == NULL)
                    min = &peer.queue[j].timeout;
                if (peer.queue[j].timeout.tv_sec < min->tv_sec ||
                    (peer.queue[j].timeout.tv_sec == min->tv_sec &&
                    peer.queue[j].timeout.tv_usec < min->tv_usec))
                    min = &peer.queue[j].timeout;
            }
        }
    }

    return min;
}

/***************************************************************************//**
*
*   Handle incoming acknowledges from all receivers
*
*   @param[in] sender The sender control block
*   @param[in] outgoing_frames_head_ptr
*
*   @return     void
*
*******************************************************************************/
void handle_incoming_acks(Sender* sender,
                          LLnode** outgoing_frames_head_ptr)
{
    //NOTE: Suggested steps for handling incoming ACKs
    //    1) Dequeue the ACK from the sender->input_framelist_head
    //    2) Convert the char * buffer to a Frame data type
    //    3) Check whether the frame is corrupted
    //    4) Check whether the frame is for this sender
    //    5) Do sliding window protocol for sender/receiver pair
    while (ll_get_length(sender->input_framelist_head) != 0)
    {
        LLnode* ll_input_frame_node = ll_pop_node(&sender->input_framelist_head);
        Frame* frame = convert_char_to_frame((UINT8*)ll_input_frame_node->value);;
        tSender_peer* peer;
        UINT16 recv_id;
        tSwpSeqno ack_num;
        tSwpSeqno i;

        if (frame->header.send_id == sender->send_id && check_CRC((UINT8*)frame))
        {
            recv_id = frame->header.recv_id;
            peer = &sender->peer[recv_id];
            ack_num = frame->header.ackNum;
            
            if(peer->LAR != ack_num)
            {
				i = peer->LAR + 1;

				while(i != ack_num)
				{
					peer->queue[i % SWS].state = tFrame_state_ack_received;
					i++;
				}
				peer->queue[i % SWS].state = tFrame_state_ack_received;

				sender_peer_open_window(peer);
            }
        }

        free(ll_input_frame_node);
        free(frame);
    }
}

/***************************************************************************//**
*
*   Handle incoming commands
*
*   @param[in] sender The sender control block
*   @param[in] outgoing_frames_head_ptr
*
*   @return     void
*
*******************************************************************************/
void handle_input_cmds(Sender* sender,
                       LLnode** outgoing_frames_head_ptr)
{
    //NOTE: Suggested steps for handling input cmd
    //    1) Dequeue the Cmd from sender->input_cmdlist_head
    //    2) Convert to Frame
    //    3) Set up the frame according to the sliding window protocol
    //    4) Compute CRC and add CRC to Frame

    int input_cmd_length = ll_get_length(sender->input_cmdlist_head);
    
        
    //Re-check the command queue length to see if stdin_thread dumped a command on us
    input_cmd_length = ll_get_length(sender->input_cmdlist_head);
    while (input_cmd_length > 0)
    {
        //Pop a node off and update the input_cmd_length
        LLnode* ll_input_cmd_node = ll_pop_node(&sender->input_cmdlist_head);
        input_cmd_length = ll_get_length(sender->input_cmdlist_head);

        //Cast to Cmd type and free up the memory for the node
        Cmd* outgoing_cmd = (Cmd*) ll_input_cmd_node->value;
        free(ll_input_cmd_node);
            
        //DUMMY CODE: Add the raw char buf to the outgoing_frames list
        //NOTE: You should not blindly send this message out!
        //      Ask yourself: Is this message actually going to the right receiver (recall that default behavior of send is to broadcast to all receivers)?
        //                    Does the receiver have enough space in in it's input queue to handle this message?
        //                    Were the previous messages sent to this receiver ACTUALLY delivered to the receiver?
        sender_send_data(sender, outgoing_cmd, outgoing_frames_head_ptr);
    }   
}

/***************************************************************************//**
*
*   Handle frames that timeout
*
*   @param[in] sender The sender control block
*   @param[in] outgoing_frames_head_ptr
*
*   @return     void
*
*******************************************************************************/
void handle_timeout_frames(Sender* sender,
                            LLnode** outgoing_frames_head_ptr)
{
    //Note: Suggested steps for handling timed out datagrams
    //    1) Iterate through the sliding window protocol information you maintain for each receiver
    //    2) Locate frames that are timed out and add them to the outgoing frames
    //    3) Update the next timeout field on the outgoing frames
    UINT32 i, j;
    tSender_peer* peer;
    tTime curr_time;

    gettimeofday(&curr_time, NULL);
    for (i = 0; i < sender->num_peers; ++i)
    {
        peer = &sender->peer[i];
        for (j = 0; j < SWS; ++j)
        {
            if (peer->queue[j].frame != NULL && peer->queue[j].state == tFrame_state_ack_waiting)
            {
                if (peer->queue[j].timeout.tv_sec < curr_time.tv_sec ||
                    (peer->queue[j].timeout.tv_sec == curr_time.tv_sec &&
                    peer->queue[j].timeout.tv_usec < curr_time.tv_usec))
                    sender_peer_resend_frame(peer, j, outgoing_frames_head_ptr);
            }
        }
    }
}

/***************************************************************************//**
*
*   Sender loop
*
*   @param[in] input_sender Pointer to the sender control block
*
*   @return     void
*
*******************************************************************************/
void* run_sender(void* input_sender)
{    
    struct timespec   time_spec;
    struct timeval    curr_timeval;
    const int WAIT_SEC_TIME = 0;
    const long WAIT_USEC_TIME = 100000;
    Sender * sender = (Sender *) input_sender;    
    LLnode * outgoing_frames_head;
    struct timeval * expiring_timeval;
    long sleep_usec_time, sleep_sec_time;
    
    //This incomplete sender thread, at a high level, loops as follows:
    //1. Determine the next time the thread should wake up
    //2. Grab the mutex protecting the input_cmd/in frame queues
    //3. Dequeues messages from the input queue and adds them to the outgoing_frames list
    //4. Releases the lock
    //5. Sends out the messages

    while(1)
    {    
        outgoing_frames_head = NULL;

        //Get the current time
        gettimeofday(&curr_timeval, 
                     NULL);

        //time_spec is a data structure used to specify when the thread should wake up
        //The time is specified as an ABSOLUTE (meaning, conceptually, you specify 9/23/2010 @ 1pm, wakeup)
        time_spec.tv_sec  = curr_timeval.tv_sec;
        time_spec.tv_nsec = curr_timeval.tv_usec * 1000;

        //Check for the next event we should handle
        expiring_timeval = sender_get_next_expiring_timeval(sender);

        //Perform full on timeout
        if (expiring_timeval == NULL)
        {
            time_spec.tv_sec += WAIT_SEC_TIME;
            time_spec.tv_nsec += WAIT_USEC_TIME * 1000;
        }
        else
        {
            //Take the difference between the next event and the current time
            sleep_usec_time = timeval_usecdiff(&curr_timeval,
                                               expiring_timeval);

            //Sleep if the difference is positive
            if (sleep_usec_time > 0)
            {
                sleep_sec_time = sleep_usec_time/1000000;
                sleep_usec_time = sleep_usec_time % 1000000;   
                time_spec.tv_sec += sleep_sec_time;
                time_spec.tv_nsec += sleep_usec_time*1000;
            }   
        }

        //Check to make sure we didn't "overflow" the nanosecond field
        if (time_spec.tv_nsec >= 1000000000)
        {
            time_spec.tv_sec++;
            time_spec.tv_nsec -= 1000000000;
        }

        
        //*****************************************************************************************
        //NOTE: Anything that involves dequeuing from the input frames or input commands should go 
        //      between the mutex lock and unlock, because other threads CAN/WILL access these structures
        //*****************************************************************************************
        pthread_mutex_lock(&sender->buffer_mutex);

        //Check whether anything has arrived
        int input_cmd_length = ll_get_length(sender->input_cmdlist_head);
        int inframe_queue_length = ll_get_length(sender->input_framelist_head);
        
        //Nothing (cmd nor incoming frame) has arrived, so do a timed wait on the sender's condition variable (releases lock)
        //A signal on the condition variable will wakeup the thread and reacquire the lock
        if (input_cmd_length == 0 &&
            inframe_queue_length == 0)
        {
            
            pthread_cond_timedwait(&sender->buffer_cv, 
                                   &sender->buffer_mutex,
                                   &time_spec);
        }
        handle_incoming_acks(sender, &outgoing_frames_head);

        handle_input_cmds(sender, &outgoing_frames_head);

        /* Send frames in the queue */
        sender_dequeue_frame(sender, &outgoing_frames_head);


        pthread_mutex_unlock(&sender->buffer_mutex);


        //Implement this
        handle_timeout_frames(sender, &outgoing_frames_head);

        //CHANGE THIS AT YOUR OWN RISK!
        //Send out all the frames
        int ll_outgoing_frame_length = ll_get_length(outgoing_frames_head);
        
        while(ll_outgoing_frame_length > 0)
        {
            LLnode * ll_outframe_node = ll_pop_node(&outgoing_frames_head);
            char * char_buf = (char *)  ll_outframe_node->value;

            //Don't worry about freeing the char_buf, the following function does that
            send_msg_to_receivers(char_buf);

            //Free up the ll_outframe_node
            free(ll_outframe_node);

            ll_outgoing_frame_length = ll_get_length(outgoing_frames_head);
        }
    }
    pthread_exit(NULL);
    return 0;
}

/***************************************************************************//**
*
*   Dequeue all the frames of the sender waiting to be sent
*
*   @param[in] sender Sender control block
*   @param[in] outgoing_frames_head_ptr
*
*   @return     void
*
*******************************************************************************/
void sender_dequeue_frame(Sender* sender, LLnode** outgoing_frames_head_ptr)
{
	UINT32 i;

	for(i = 0; i < sender->num_peers; ++i)
	{
		sender_peer_dequeue_frame(&sender->peer[i], outgoing_frames_head_ptr);
	}
}

/***************************************************************************//**
*
*   Dequeue all the frames addressed to the peer
*
*   @param[in] peer Sender peer control block
*   @param[in] outgoing_frames_head_ptr
*
*   @return     void
*
*******************************************************************************/
void sender_peer_dequeue_frame(tSender_peer* peer, LLnode** outgoing_frames_head_ptr)
{
	LLnode* frame_node;
	Frame* frame;
	UINT32 length = ll_get_length(peer->frame_buffer);

	while(sender_peer_there_is_window_available(peer) && length != 0)
	{
		frame_node = ll_pop_node(&peer->frame_buffer);
		frame = (Frame*)frame_node->value;
		sender_peer_dispatch_frame(peer, outgoing_frames_head_ptr, frame);

		free(frame_node);
		length = ll_get_length(peer->frame_buffer);
	}
}

/***************************************************************************//**
*
*   Send the data in a command
*
*   @param[in] sender Sender control block
*   @param[in] outgoing_cmd	Command to be sent
*   @param[out] outgoing_frames_head_ptr	Frames to be sent to receiver
*
*   @return     void
*
*******************************************************************************/
void sender_send_data(Sender *sender, Cmd* outgoing_cmd, 
    LLnode** outgoing_frames_head_ptr)
{
    UINT32 msg_length = strlen(outgoing_cmd->message);
    UINT32 fragments = (msg_length % FRAME_PAYLOAD_SIZE == 0) ?
        msg_length / FRAME_PAYLOAD_SIZE : msg_length / FRAME_PAYLOAD_SIZE + 1;

    if (fragments > MAX_FRAGMENTS)
    {
        fprintf(stderr, "Message too long. The limit for fragments is %d frames!", MAX_FRAGMENTS);
        free(outgoing_cmd);
        return;
    }

    sender_send_fragments(sender, outgoing_cmd, outgoing_frames_head_ptr, fragments);
}

/***************************************************************************//**
*
*   Send the data in a command
*
*   @param[in] sender 						Sender control block
*   @param[in] cmd							Command to be sent
*   @param[out] outgoing_frames_head_ptr	Frames to be sent to receiver
*   @param[in] fragments					Number of fragments the message should
*   											be broken
*
*   @return     void
*
*******************************************************************************/
void sender_send_fragments(Sender* sender, Cmd* cmd, LLnode** outgoing_frames_head_ptr, UINT32 fragments)
{
	UINT8 i;
	UINT16 dst_id = cmd->dst_id;
	UINT16 src_id = cmd->src_id;
    tSender_peer *peer = &sender->peer[dst_id];
    Frame* frame;

    peer->LPC++;

    for (i = 0; i < fragments; ++i)
	{
    	frame = create_frame(src_id, dst_id, tHeader_flag_data);
        frame->header.seqnum = ++(peer->LFC);
        frame->header.fragment_id = i;
        frame->header.package_id = peer->LPC;
        frame->header.num_fragments = fragments;
        strncpy((char*)frame->data, cmd->message + i * FRAME_PAYLOAD_SIZE, FRAME_PAYLOAD_SIZE);
        frame->crc = 0x0;
        frame->crc = CRC((UINT8*)frame, MAX_FRAME_SIZE);

        /* Enqueue frames to be sent in the future */
        ll_append_node(&peer->frame_buffer, frame);
	}

    // At this point, we don't need the command
	free(cmd->message);
	free(cmd);
}

/***************************************************************************//**
*
*   Check if there is window available
*
*   @param[in] peer Sender peer control block
*
*   @return     TRUE if there is window available; FALSE otherwise
*
*******************************************************************************/
BOOLEAN sender_peer_there_is_window_available(tSender_peer* peer)
{
	if(sender_peer_window_wrap(peer))
	{
		return peer->LFS + 1 + MAX_SEQ_NUM - peer->LAR  < SWS ? TRUE : FALSE;
	} else {
		return peer->LFS - peer->LAR < SWS ? TRUE : FALSE;
	}
}

/***************************************************************************//**
*
*   Check if the peer frame sequence number had wrapped around
*
*   @param[in] peer Sender peer control block
*
*   @return     TRUE if there sequence number wrapped around; FALSE otherwise
*
*******************************************************************************/
BOOLEAN sender_peer_window_wrap(tSender_peer* peer)
{
	if(peer->LFS >= peer->LAR)
		return FALSE;
	else
		return TRUE;
}

/***************************************************************************//**
*
*   Send a frame to the receiver
*
*   @param[in] peer Sender peer control block
*   @param[out] outgoing_frames_head_ptr
*   @param[in] frame
*
*   @return     void
*
*******************************************************************************/
void sender_peer_dispatch_frame(tSender_peer* peer, LLnode** outgoing_frames_head_ptr, Frame* frame)
{
	UINT8 slot;
	UINT8* outgoing_charbuf;

    // Convert the message to the outgoing_charbuf
    outgoing_charbuf = convert_frame_to_char(frame);
    ll_append_node(outgoing_frames_head_ptr, outgoing_charbuf);
    slot = frame->header.seqnum % SWS;
    peer->queue[slot].frame = frame;
    peer->queue[slot].state = tFrame_state_ack_waiting;
    sender_peer_set_frame_timeout(peer, slot);
    peer->LFS++;

#if (DEBUG == 1)
    fprintf(stderr, "<SENDER_%d> -------> <RECV_%d> [%s]\n", frame->header.send_id, frame->header.recv_id, frame->data);
#endif
}

/***************************************************************************//**
*
*   Resend a frame to the receiver
*
*   @param[in] peer Sender peer control block
*   @param[out] window_slot	index in the peer control block where is the frame to
*   	be resent
*   @param[out] outgoing_frames_head_ptr
*
*   @return     void
*
*******************************************************************************/
void sender_peer_resend_frame(tSender_peer* peer, UINT8 window_slot, LLnode **outgoing_frames_head_ptr)
{
    UINT8* outgoing_charbuf = convert_frame_to_char(peer->queue[window_slot].frame);
    ll_append_node(outgoing_frames_head_ptr, outgoing_charbuf);
    sender_peer_set_frame_timeout(peer, window_slot);

#if (DEBUG == 2)
    Frame* frame = peer->queue[window_slot].frame;
    fprintf(stderr, "<SENDER_%d> ---x     <RECV_%d> [%s] timeout frame %d  time %d:%d\n", frame->header.send_id,
    		frame->header.recv_id, frame->data,
			(UINT32)frame->header.seqnum, (UINT32)peer->queue[window_slot].timeout.tv_sec % 60, (UINT32) peer->queue[window_slot].timeout.tv_usec);
#endif
}

/***************************************************************************//**
*
*   Set new timeout to a frame
*
*   @param[in] peer Sender peer control block
*   @param[out] window_slot	index in the peer control block where is the frame to
*   	have the timeout reseted
*
*   @return     void
*
*******************************************************************************/
void sender_peer_set_frame_timeout(tSender_peer* peer, UINT8 window_slot)
{
    tTime curr_time;

    gettimeofday(&curr_time, NULL);
    peer->queue[window_slot].timeout.tv_sec = curr_time.tv_sec + (curr_time.tv_usec + TIMETOUT_USEC) / 1000000;
    peer->queue[window_slot].timeout.tv_usec = (curr_time.tv_usec + TIMETOUT_USEC) % 1000000;
}

/***************************************************************************//**
*
*   Check if there is frames that receveid ACK and open the window
*
*   @param[in] peer Sender peer control block
*
*   @return     void
*
*******************************************************************************/
void sender_peer_open_window(tSender_peer* peer)
{
	UINT8 slot = (peer->LAR + 1) % SWS;
	Frame* frame = peer->queue[slot].frame;

	while(frame != NULL && peer->queue[slot].state == tFrame_state_ack_received)
	{
#if (DEBUG == 2)
		fprintf(stderr, "<SENDER_%d> <------- <RECV_%d> Received ACK %d\n", frame->header.send_id, frame->header.recv_id, frame->header.seqnum);
#endif
		free(peer->queue[slot].frame);
		peer->queue[slot].frame = NULL;
		peer->queue[slot].state = tFrame_state_empty;
		peer->LAR++;
		slot = (peer->LAR + 1) % SWS;
		frame = peer->queue[slot].frame;
	}
}
