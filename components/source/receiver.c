#include "receiver.h"
#include <assert.h>

/**< @file
@brief Receiver function definitions. */

/* Private Functions */
static void handle_incoming_msgs(Receiver* receiver, LLnode** outgoing_frames_head_ptr);
static void init_receiver_peer(tReceiver_peer* peer);
static BOOLEAN receiver_window_wrap_around(tReceiver_peer* peer);
static BOOLEAN receiver_frame_already_received(Receiver* receiver, Frame *frame);
static BOOLEAN receiver_frame_out_of_order(Receiver* receiver, Frame* frame);
static BOOLEAN receiver_frame_out_of_window(Receiver* receiver, Frame* frame);
static void receiver_dispatch_msg(Receiver* receiver, Frame* frame);
static void receiver_send_ack(Frame* frame, UINT8* payload, LLnode** outgoing_frames_head_ptr);
static void receiver_queue_msg(Receiver* receiver, Frame* frame, LLnode** outgoing_frames_head_ptr);
static void receiver_dequeue_msg(Receiver* receiver, LLnode** outgoing_frames_head_ptr);

/***************************************************************************//**
*
*   Initialize the receiver control block
*
*   @param[in] receiver 	The receiver control block
*   @param[in] ID			Sender ID
*   @param[in] senders 		The number of senders the sender should keep connection
*
*   @return     void
*
*******************************************************************************/
void init_receiver(Receiver* receiver, tReceiverID ID, UINT32 senders)
{
	UINT8 i;

	memset(receiver, 0, sizeof(Receiver));
    receiver->recv_id = ID;
    receiver->input_framelist_head = NULL;
    receiver->num_peers = senders;
    receiver->peer = malloc(sizeof(tReceiver_peer) * senders);

    for (i = 0; i < senders; ++i)
    	init_receiver_peer(&receiver->peer[i]);

    pthread_cond_init(&receiver->buffer_cv, NULL);
    pthread_mutex_init(&receiver->buffer_mutex, NULL);
}

/***************************************************************************//**
*
*   Initialize the receiver peer control block
*
*   @param[in] peer 		The receiver peer control block
*
*   @return     void
*
*******************************************************************************/
void init_receiver_peer(tReceiver_peer* peer)
{
	UINT8 i;

	memset(peer, 0, sizeof(tReceiver_peer));
	peer->NFE = 0;
	peer->LEF_recv = NULL;
	for(i = 0; i < SWS; ++i){
		peer->queue[i].msg = NULL;
		peer->queue[i].state = tFrame_state_empty;
	}
}

/***************************************************************************//**
*
*   Handle incoming messages from senders
*
*   @param[in] receiver 		The receiver control block
*   @param[out] outgoing_frames_head_ptr
*
*   @return     void
*
*******************************************************************************/
void handle_incoming_msgs(Receiver* receiver,
                          LLnode** outgoing_frames_head_ptr)
{
	tReceiver_peer* peer;
    //NOTE: Suggested steps for handling incoming frames
    //    1) Dequeue the Frame from the sender->input_framelist_head
    //    2) Convert the char * buffer to a Frame data type
    //    3) Check whether the frame is corrupted
    //    4) Check whether the frame is for this receiver
    //    5) Do sliding window protocol for sender/receiver pair

    int incoming_msgs_length = ll_get_length(receiver->input_framelist_head);
    while (incoming_msgs_length > 0)
    {
        //Pop a node off the front of the link list and update the count
        LLnode* ll_inmsg_node = ll_pop_node(&receiver->input_framelist_head);
        incoming_msgs_length = ll_get_length(receiver->input_framelist_head);

        //NOTE: You should not blindly print messages!
        //      Ask yourself: Is this message really for me?
        //                    Is this message corrupted?
        //                    Is this an old, retransmitted message?           
        UINT8* raw_char_buf = (UINT8*) ll_inmsg_node->value;
        Frame* inframe = convert_char_to_frame((UINT8*)raw_char_buf);
        
        if(inframe->header.recv_id == receiver->recv_id)
        {
			if(check_CRC((UINT8*)inframe) == TRUE){
				/* The ACK could have been lost */
#if (DEBUG == 10)
				fprintf(stderr, "[%s] %d\n", inframe->data, inframe->header.seqnum);
#endif
				if(receiver_frame_already_received(receiver, inframe)){
					/* Send ACK for the last frame expected received */
					peer = &receiver->peer[inframe->header.send_id];
					receiver_send_ack(peer->LEF_recv, inframe->data, outgoing_frames_head_ptr);
					free(inframe);
				} else if(receiver_frame_out_of_order(receiver, inframe)){
					/* Queue frame */
					receiver_queue_msg(receiver, inframe, outgoing_frames_head_ptr);
				} else {
					/* Queue frame to dispatch latter */
					receiver_queue_msg(receiver, inframe, outgoing_frames_head_ptr);
				}
        	} else {
#if(DEBUG == 2)
        		fprintf(stderr, "<RECV_%d> CORRUPTED!!!\n", receiver->recv_id);
#endif
                /* Free frame */
            	free(inframe);
        	}
        }

        // Free raw_char_buf
        free(raw_char_buf);
        free(ll_inmsg_node);
    }
}

/***************************************************************************//**
*
*   The receiver main loop
*
*   @param[in] input_receiver 		Pointer to the receiver control block
*
*   @return     void
*
*******************************************************************************/
void* run_receiver(void* input_receiver)
{    
    struct timespec   time_spec;
    struct timeval    curr_timeval;
    const int WAIT_SEC_TIME = 0;
    const long WAIT_USEC_TIME = 100000;
    Receiver * receiver = (Receiver *) input_receiver;
    LLnode * outgoing_frames_head;


    //This incomplete receiver thread, at a high level, loops as follows:
    //1. Determine the next time the thread should wake up if there is nothing in the incoming queue(s)
    //2. Grab the mutex protecting the input_msg queue
    //3. Dequeues messages from the input_msg queue and prints them
    //4. Releases the lock
    //5. Sends out any outgoing messages

    while(1)
    {    
        //NOTE: Add outgoing messages to the outgoing_frames_head pointer
        outgoing_frames_head = NULL;
        gettimeofday(&curr_timeval, 
                     NULL);

        //Either timeout or get woken up because you've received a datagram
        //NOTE: You don't really need to do anything here, but it might be useful for debugging purposes to have the receivers periodically wakeup and print info
        time_spec.tv_sec  = curr_timeval.tv_sec;
        time_spec.tv_nsec = curr_timeval.tv_usec * 1000;
        time_spec.tv_sec += WAIT_SEC_TIME;
        time_spec.tv_nsec += WAIT_USEC_TIME * 1000;
        if (time_spec.tv_nsec >= 1000000000)
        {
            time_spec.tv_sec++;
            time_spec.tv_nsec -= 1000000000;
        }

        //*****************************************************************************************
        //NOTE: Anything that involves dequeuing from the input frames should go 
        //      between the mutex lock and unlock, because other threads CAN/WILL access these structures
        //*****************************************************************************************
        pthread_mutex_lock(&receiver->buffer_mutex);

        //Check whether anything arrived
        int incoming_msgs_length = ll_get_length(receiver->input_framelist_head);
        if (incoming_msgs_length == 0)
        {
            //Nothing has arrived, do a timed wait on the condition variable (which releases the mutex). Again, you don't really need to do the timed wait.
            //A signal on the condition variable will wake up the thread and reacquire the lock
            pthread_cond_timedwait(&receiver->buffer_cv, 
                                   &receiver->buffer_mutex,
                                   &time_spec);
        }
        handle_incoming_msgs(receiver, &outgoing_frames_head);

        receiver_dequeue_msg(receiver, &outgoing_frames_head);


        pthread_mutex_unlock(&receiver->buffer_mutex);
        
        //CHANGE THIS AT YOUR OWN RISK!
        //Send out all the frames user has appended to the outgoing_frames list
        int ll_outgoing_frame_length = ll_get_length(outgoing_frames_head);
        while(ll_outgoing_frame_length > 0)
        {
            LLnode * ll_outframe_node = ll_pop_node(&outgoing_frames_head);
            char * char_buf = (char *) ll_outframe_node->value;
            
            //The following function frees the memory for the char_buf object
            send_msg_to_senders(char_buf);

            //Free up the ll_outframe_node
            free(ll_outframe_node);

            ll_outgoing_frame_length = ll_get_length(outgoing_frames_head);
        }
    }
    pthread_exit(NULL);

}

/***************************************************************************//**
*
*   Check if the sequence number of a connection of the receiver with a
*   sender had wrapped around.
*
*   @param[in] peer 	The peer control block
*
*   @return     		True if the window wrapped around
*
*******************************************************************************/
BOOLEAN receiver_window_wrap_around(tReceiver_peer* peer)
{
	if(peer->NFE + RWS - 1 < peer->NFE)
		return TRUE;
	return FALSE;
}

/***************************************************************************//**
*
*   Check if a frame was already received. We consider the frame as already
*   received if it is out of the window.
*
*   @param[in] receiver The receiver control block
*   @param[in] frame	The received frame
*
*   @return    			True if the frame was already received
*
*******************************************************************************/
BOOLEAN receiver_frame_already_received(Receiver* receiver, Frame *frame)
{
	return receiver_frame_out_of_window(receiver, frame) ? TRUE : FALSE;
}

/***************************************************************************//**
*
*   Check if a frame is the next frame expected.
*
*   @param[in] receiver The receiver control block
*   @param[in] frame	The received frame
*
*   @return     		True if the frame is not the next expected
*
*******************************************************************************/
BOOLEAN receiver_frame_out_of_order(Receiver* receiver, Frame* frame)
{
	tSwpSeqno seq_num = frame->header.seqnum;
	UINT16 peer_id = frame->header.send_id;
	tReceiver_peer* peer = &receiver->peer[peer_id];

	if(peer->NFE != seq_num)
		return TRUE;
	return FALSE;
}

/***************************************************************************//**
*
*   Check if a frame is in the receiver window.
*
*   @param[in] receiver The receiver control block
*   @param[in] frame	The received frame
*
*   @return     		True if the frame is in the receiver's window
*
*******************************************************************************/
BOOLEAN receiver_frame_out_of_window(Receiver* receiver, Frame* frame){
	tSwpSeqno seq_num = frame->header.seqnum;
	UINT16 peer_id = frame->header.send_id;
	tReceiver_peer* peer = &receiver->peer[peer_id];

	if(receiver_window_wrap_around(peer)){
		if(seq_num < peer->NFE && seq_num >= peer->NFE + RWS){
			return TRUE;
		}
		return FALSE;
	} else {
		if(seq_num >= peer->NFE + RWS || seq_num < peer->NFE){
			return TRUE;
		}
		return FALSE;
	}
}

/***************************************************************************//**
*
*   Accept a message. Check if the frame is the next frame expected.
*
*   @param[in] receiver The receiver control block
*   @param[in] frame	The received frame
*
*   @return     		void
*
*******************************************************************************/
void receiver_dispatch_msg(Receiver* receiver, Frame* frame)
{
	UINT16 peer_id = frame->header.send_id;
	tReceiver_peer* peer = &receiver->peer[peer_id];

	if(frame->header.seqnum == peer->NFE){
		fprintf(stdout, "<RECV_%d>:[%s]\n", (UINT32)receiver->recv_id, frame->data);
		free(peer->LEF_recv);
		peer->LEF_recv = frame;
		peer->NFE++;
	}
	else
	{
		fprintf(stderr, "CAN ONLY DISPATCH MSGS IN ORDER");
#if (DEBUG_ASSERT == TRUE)
		assert(0);
#endif
	}
}

/***************************************************************************//**
*
*   Enqueue a message received out of order.
*
*   @param[in] receiver The receiver control block
*   @param[in] frame	The received frame
*   @param[out] outgoing_frames_head_ptr	List of ACKs to send to the sender
*
*   @return     void
*
*******************************************************************************/
void receiver_queue_msg(Receiver* receiver, Frame* frame, LLnode** outgoing_frames_head_ptr)
{
	UINT16 peer_id = frame->header.send_id;
	tReceiver_peer* peer = &receiver->peer[peer_id];
	tSwpSeqno seq_num = frame->header.seqnum;
	UINT8 slot = seq_num % RWS;

	peer->queue[slot].msg = frame;
	peer->queue[slot].state = tFrame_state_out_of_order;
}

/***************************************************************************//**
*
*   Dequeue messages that were received out of order.
*
*   @param[in] receiver 					The receiver control block
*	@param[out] outgoing_frames_head_ptr	List of ACKs frames to send to the sender
*
*   @return     void
*
*******************************************************************************/
void receiver_dequeue_msg(Receiver* receiver, LLnode** outgoing_frames_head_ptr)
{
	tReceiver_peer* peer;
	Frame* frame;
	UINT32 peer_id;
	UINT8 slot;
	BOOLEAN flag;

	for(peer_id = 0; peer_id < receiver->num_peers; ++peer_id)
	{
		peer = &receiver->peer[peer_id];
		slot = peer->NFE % RWS;
		frame = peer->queue[slot].msg;
		flag = FALSE;

		while(frame != NULL && frame->header.seqnum == peer->NFE)
		{
			receiver_dispatch_msg(receiver, frame);
			flag = TRUE;
			peer->queue[slot].msg = NULL;
			peer->queue[slot].state = tFrame_state_empty;
			slot = peer->NFE % RWS;
			frame = peer->queue[slot].msg;
		}
		if(flag)
			receiver_send_ack(peer->LEF_recv, peer->LEF_recv->data, outgoing_frames_head_ptr);
	}
}

/***************************************************************************//**
*
*   Send ACK for a given frame
*
*   @param[in] frame						The frame to be acknowledged
*   @param[in] payload						The payload to be printed for debugging
*   @param[out] outgoing_frames_head_ptr	List of ACKs frames to send to the sender
*
*   @return     void
*
*******************************************************************************/
void receiver_send_ack(Frame* frame, UINT8* payload, LLnode** outgoing_frames_head_ptr)
{
	tSwpSeqno seq_num = frame->header.seqnum;
	UINT8* outgoing_charbuf;

	frame->header.ackNum = seq_num;
	frame->header.flag = tHeader_flag_ack;
	frame->crc = 0x00;
	frame->crc = CRC((UINT8*)frame, MAX_FRAME_SIZE);

	outgoing_charbuf = convert_frame_to_char(frame);
	ll_append_node(outgoing_frames_head_ptr, outgoing_charbuf);

#if (DEBUG == 2)
    fprintf(stderr, "<SENDER_%d> <------- <RECV_%d> [%s] ACK %d\n", frame->header.send_id, frame->header.recv_id, payload, seq_num);
#endif
}
