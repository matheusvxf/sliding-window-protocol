#include "util.h"

/**< @file
@brief Utility functions definition. */

//Linked list functions
int ll_get_length(LLnode * head)
{
    LLnode * tmp;
    int count = 1;
    if (head == NULL)
        return 0;
    else
    {
        tmp = head->next;
        while (tmp != head)
        {
            count++;
            tmp = tmp->next;
        }
        return count;
    }
}

void ll_append_node(LLnode ** head_ptr, 
                    void * value)
{
    LLnode * prev_last_node;
    LLnode * new_node;
    LLnode * head;

    if (head_ptr == NULL)
    {
        return;
    }
    
    //Init the value pntr
    head = (*head_ptr);
    new_node = (LLnode *) malloc(sizeof(LLnode));
    new_node->value = value;

    //The list is empty, no node is currently present
    if (head == NULL)
    {
        (*head_ptr) = new_node;
        new_node->prev = new_node;
        new_node->next = new_node;
    }
    else
    {
        //Node exists by itself
        prev_last_node = head->prev;
        head->prev = new_node;
        prev_last_node->next = new_node;
        new_node->next = head;
        new_node->prev = prev_last_node;
    }
}


LLnode * ll_pop_node(LLnode ** head_ptr)
{
    LLnode * last_node;
    LLnode * new_head;
    LLnode * prev_head;

    prev_head = (*head_ptr);
    if (prev_head == NULL)
    {
        return NULL;
    }
    last_node = prev_head->prev;
    new_head = prev_head->next;

    //We are about to set the head ptr to nothing because there is only one thing in list
    if (last_node == prev_head)
    {
        (*head_ptr) = NULL;
        prev_head->next = NULL;
        prev_head->prev = NULL;
        return prev_head;
    }
    else
    {
        (*head_ptr) = new_head;
        last_node->next = new_head;
        new_head->prev = last_node;

        prev_head->next = NULL;
        prev_head->prev = NULL;
        return prev_head;
    }
}

void ll_destroy_node(LLnode * node)
{
    if (node->type == llt_string)
    {
        free((char *) node->value);
    }
    free(node);
}

// Compute the difference in usec for two timeval objects
long timeval_usecdiff(struct timeval *start_time, 
                      struct timeval *finish_time)
{
  long usec;
  usec=(finish_time->tv_sec - start_time->tv_sec)*1000000;
  usec+=(finish_time->tv_usec- start_time->tv_usec);
  return usec;
}

// Print out messages entered by the user
void print_cmd(Cmd * cmd)
{
    fprintf(stderr, "src=%d, dst=%d, message=%s\n", 
           cmd->src_id,
           cmd->dst_id,
           cmd->message);
}

/***************************************************************************//**
*
*   Serialize a frame. Convert the frame to a binary representation
*
*   @param[in] frame 	The frame
*
*   @return     A pointer to a buffer with a copy of the frame
*
*******************************************************************************/
UINT8* convert_frame_to_char(Frame* frame)
{
    //NOTE: You should implement this as necessary
    UINT8* char_buffer = (UINT8*) malloc(sizeof(Frame));
    memset(char_buffer, 0, sizeof(Frame));

    /* Serialize */
    memcpy(char_buffer, frame, sizeof(Frame));
    return char_buffer;
}

/***************************************************************************//**
*
*   Deserialize a frame. Convert the content of char buffer to a frame
*
*   @param[in] char_buffer 	The char buffer
*
*   @return     A pointer to a frame
*
*******************************************************************************/
Frame* convert_char_to_frame(UINT8* char_buffer)
{
    //NOTE: You should implement this as necessary
    Frame* frame = (Frame*) malloc(sizeof(Frame));
    memset(frame, 0, sizeof(Frame));

    /* Des serialize */
    memcpy(frame, char_buffer, sizeof(Frame));
    return frame;
}

/***************************************************************************//**
*
*   Create a new frame
*
*   @param[in] sender 	The sender ID
*   @param[in] receiver	The receiver ID
*   @param[in] flag		The frame flags
*
*   @return     A pointer to a frame
*
*******************************************************************************/
Frame* create_frame(tSenderID sender, tReceiverID receiver, tHeader_flag flag)
{
    Frame* frame = (Frame*)malloc(sizeof(Frame));
    memset(frame, 0, sizeof(Frame));
    frame->header.send_id = sender;
    frame->header.recv_id = receiver;
    frame->header.flag = flag;

    return frame;
}

#define shift_left_by_one(a) 	(a << 1)
#define shift_right_by_one(a) 	(a >> 1)

UINT8 get_bit(UINT8 pos, UINT64 byte)
{
	return (byte & (0x1 << pos)) ? 0x1 : 0x0;
}

#if (USE_CRC_32 == TRUE)

/***************************************************************************//**
*
*   Generate the CRC 32 of a char buffer.
*
*   @param[in] array 	Array of bytes
*   @param[in] n_bytes	Number of bytes in the array
*
*   @return     the CRC 32
*
*******************************************************************************/
tCRC_32 CRC(UINT8* array, UINT32 n_bytes)
{
	UINT32 generator = CRC_32 >> 1;
	tCRC_32 crc = 0x0;
    INT8 i, j;
    UINT32* p_uint32;
    UINT32 next_uint32;
    UINT32 n_uint32;

    p_uint32 = (UINT32*)array;
    n_uint32 = n_bytes / 4;

    for(i = 0; i < n_uint32; ++i)
    {
    	next_uint32 = p_uint32[i];

    	for(j = 31; j >= 0; --j){
			if(get_bit(31, crc) == 0)
			{
				crc = crc << 1; // shift out 0
				crc = crc | get_bit(j, next_uint32); // shift in next bit
			}
			else{
				crc = (crc ^ generator) << 1;
				crc = crc | (get_bit(j, next_uint32) ^ 1);
			}
    	}
    }
    return crc;
}

#else

/***************************************************************************//**
*
*   Generate the CRC 8 of a char buffer.
*
*   @param[in] array 	Array of bytes
*   @param[in] n_bytes	Number of bytes in the array
*
*   @return     the CRC 8
*
*******************************************************************************/
tCRC_8 CRC(UINT8* array, UINT32 n_bytes)
{
	UINT8 generator = CRC_8 >> 1;
	tCRC_8 crc = 0x0;
    INT8 i, j;
    UINT8 next_byte;

    for(i = 0; i < n_bytes; ++i)
    {
    	next_byte = array[i];

    	for(j = 7; j >= 0; --j){
			if(get_bit(7, crc) == 0)
			{
				crc = crc << 1; // shift out 0
				crc = crc | get_bit(j, next_byte); // shift in next bit
			}
			else{
				crc = (crc ^ generator) << 1;
				crc = crc | (get_bit(j, next_byte) ^ 1);
			}
    	}
    }
    return crc;
}

#endif /* USE_CRC_32 */

BOOLEAN check_CRC(UINT8 msg[MAX_FRAME_SIZE])
{
	if(CRC(msg, MAX_FRAME_SIZE) == 0){
		return TRUE;
	} else {
		return FALSE;
	}
}
