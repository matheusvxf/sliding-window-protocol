#ifndef __COMMON_H__
#define __COMMON_H__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <unistd.h>
#include <netdb.h>
#include <math.h>
#include <sys/time.h>

#define TIMETOUT_USEC 10000
#define MAX_COMMAND_LENGTH 16   /**< Maximum command length */
#define AUTOMATED_FILENAME 512
#define MAX_FRAME_SIZE 64       /**< Maximum frame size */
#define MAX_FRAGMENTS  256      /**< Maximum number of fragments a message can be split */
#define MAX_SEQ_NUM		255		/**< MAximum sequence number */
#define SWS 8                   /**< Sender window size */
#define RWS 8					/**< Receiver window size */


/* Data Types */
typedef unsigned char           uchar_t;
typedef unsigned char           UINT8;
typedef unsigned short          UINT16;
typedef unsigned int            UINT32;
typedef unsigned long long int  UINT64;
typedef signed char             INT8;
typedef signed short            INT16;
typedef signed int              INT32;
typedef signed long long int    INT64;
typedef unsigned char           BOOLEAN;
typedef struct timeval          tTime;
#define TRUE    1
#define FALSE   0

#define CRC_8	((1 << 8) | (1 << 2) | (1 << 1) | (1))
#define CRC_32 (((UINT64)1 << 32) | (1 << 26) | (1 << 23) | (1 << 22) | (1 << 16) \
                |(1 << 12) | (1 << 11) | (1 << 10) | (1 << 8)  | (1 << 7)  \
                |(1 << 5)  | (1 << 4)  | (1 << 2)  | (1 << 1)  | (1))

/* Compilation flags */
#define DEBUG 1
#define DEBUG_ASSERT FALSE
#define USE_CRC_32 TRUE

//System configuration information
struct SysConfig_t
{
    float drop_prob;
    float corrupt_prob;
    unsigned char automated;
    char automated_file[AUTOMATED_FILENAME];
};
typedef struct SysConfig_t  SysConfig;

//Command line input information
struct Cmd_t
{
    uint16_t src_id;
    uint16_t dst_id;
    char * message;
};
typedef struct Cmd_t Cmd;

//Linked list information
enum LLtype 
{
    llt_string,
    llt_frame,
    llt_integer,
    llt_head
} LLtype;

struct LLnode_t
{
    struct LLnode_t * prev;
    struct LLnode_t * next;
    enum LLtype type;

    void * value;
};
typedef struct LLnode_t LLnode;

typedef enum tHeader_flag
{
	tHeader_flag_data 	= 0x01, /**< Indicate the message is data */
	tHeader_flag_ack 	= 0x02, /**< Indicate the message is an acknowledgment */
	tHeader_flag_frag	= 0x04	/**< Indicate the message was fragmented */
} tHeader_flag; /**< Flag bit mask */

#define header_is_data(a) 		(a.flag & tHeader_flag_data)
#define header_is_ack(a)		(a.flag & tHeader_flag_ack)
#define header_is_frag(a)		(a.flag & tHeader_flag_frag)	/**< Return true if the frame is a fragment */

typedef enum tFrame_state
{
	tFrame_state_empty 			= 0x00,
	tFrame_state_ack_received 	= 0x01,
	tFrame_state_ack_waiting  	= 0x02,
	tFrame_state_out_of_order 	= 0x04
} tFrame_state;	/**< Frame state bit mask */

typedef UINT16 tSenderID;	/**< Sender ID data type */
typedef UINT16 tReceiverID;	/**< Receiver ID data type */
typedef UINT8 tSwpSeqno;	/**< Frame sequence number data type */

/* CRC definition */
#if (USE_CRC_32 == TRUE)

typedef UINT32 tCRC_32;		/**< CRC 32 bits data type */
#define CRC_SIZE    (sizeof(tCRC_32)) 	/** CRC (Cyclic Redundancy Check) size */

#else

typedef UINT8 tCRC_8;		/**< CRC 8 bits data type */
#define CRC_SIZE	(sizeof(tCRC_8))	/** CRC (Cyclic Redundancy Check) size */

#endif /* USE_CRC_32 */

typedef struct
{
    tSenderID      	send_id;		/**< ID of the sender */
    tReceiverID   	recv_id;		/**< ID of the receiver */
    tSwpSeqno   	seqnum;			/**< Sequence number of the frame - range [0,255] */
    tSwpSeqno   	ackNum;			/**< Sequence number of the frame being acknowledged */
    tHeader_flag 	flag;			/**< Flags for the header */

    /* In case of fragmented frames */
    UINT8		package_id;		/**< Package number used to join fragments */
    UINT8       fragment_id;	/**< Fragment ID used to know the order of the frame in the package */
    UINT8 		num_fragments;	/**< How many fragments the package was split */
} tSwpHdr;						/**< Slide window protocol frame header */


#define SWP_HEADER_SIZE (sizeof(tSwpHdr)) /**< Slide windows protocol header size */
#define FRAME_PAYLOAD_SIZE (MAX_FRAME_SIZE - SWP_HEADER_SIZE - CRC_SIZE - 1)   /**< Maximum payload size in each frame */
#define MSG_SIZE (MAX_FRAME_SIZE - CRC_SIZE) /**< Message size excluding the CRC */

typedef struct
{
    tSwpHdr     header;	/**< Header of the frame */
    UINT8       data[FRAME_PAYLOAD_SIZE];	/**< Buffer for the message being sent */
#if (USE_CRC_32 == TRUE)
    tCRC_32     crc;	/**< CRC 32 for error detection */
#else
    tCRC_8 		crc;	/**< CRC 8 for error detection */
#endif
} Frame;				/**< Frame data structure */

/* Receiver and sender data structures */

typedef struct
{
    tSwpSeqno   NFE;    	/**< Next frame expected */
    Frame* 		LEF_recv;	/**< Last expected frame received */

    struct tRcv_queue_slot
    {
        Frame*  msg;		/**< Pointer to the frame received */
        tFrame_state state;	/**< Frame state in the receiver */
    } queue[SWS];       	/**< Enqueue messages received out of order */
} tReceiver_peer;			/**< Receiver peer control block */

typedef struct
{
    //DO NOT CHANGE:
    // 1) buffer_mutex
    // 2) buffer_cv
    // 3) input_framelist_head
    // 4) recv_id
    pthread_mutex_t buffer_mutex;
    pthread_cond_t buffer_cv;
    LLnode* input_framelist_head;
    tReceiverID recv_id;			/**< Receiver ID */

    /* SWP control block */
    UINT32 num_peers;		/**< Amount of Sender the receiver is connected to */
    tReceiver_peer* peer;	/**< Control block for each one of the peers the receiver can receive messages */
} Receiver;             	/**< Receiver control block */

typedef struct tSender_queue_slot
{
    tTime timeout;		/**< Maximum instant of time to wait until receive the ACK */
    Frame* frame;		/**< Frame pointer - used to retransmission in case of timeout */
    tFrame_state state;	/**< State of the window slot */
} tSender_queue_slot;	/**< Slide window protocol frame slot */

typedef struct
{
    tSwpSeqno   LAR;    /**< Sequence number of the last ACK received */
    tSwpSeqno   LFS;    /**< Sequence number of the last frame sent */
    tSwpSeqno   LFC;    /**< Last frame created - Sent or stored in the buffer */
    UINT8		LPC;	/**< Last package created */

    LLnode*     frame_buffer; 		/**< List of frames to be sent by this peer */

    tSender_queue_slot queue[SWS];	/**< Circular array for the frames sent and waiting ACK */
} tSender_peer; /**< Sender peers control block */

typedef struct
{
    //DO NOT CHANGE:
    // 1) buffer_mutex
    // 2) buffer_cv
    // 3) input_cmdlist_head
    // 4) input_framelist_head
    // 5) send_id
    pthread_mutex_t buffer_mutex;
    pthread_cond_t buffer_cv;
    LLnode* input_cmdlist_head;
    LLnode* input_framelist_head;
    UINT32 send_id;		/**< Sender id */

    /* SWP control block */
    UINT32 num_peers;	/**< Number of receivers we are connected to */
    tSender_peer* peer; /**< Control block for each one of the peers the sender can send messages */
} Sender; 				/**< Sender control block */

enum SendFrame_DstType 
{
    ReceiverDst,
    SenderDst
} SendFrame_DstType ;

//Declare global variables here
//DO NOT CHANGE: 
//   1) glb_senders_array
//   2) glb_receivers_array
//   3) glb_senders_array_length
//   4) glb_receivers_array_length
//   5) glb_sysconfig
//   6) CORRUPTION_BITS
Sender * glb_senders_array;
Receiver * glb_receivers_array;
int glb_senders_array_length;
int glb_receivers_array_length;
SysConfig glb_sysconfig;
int CORRUPTION_BITS;
#endif 
