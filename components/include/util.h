#ifndef __UTIL_H__
#define __UTIL_H__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <unistd.h>
#include <netdb.h>
#include <math.h>
#include <sys/time.h>
#include "common.h"

//Linked list functions
int ll_get_length(LLnode *);
void ll_append_node(LLnode **, void *);
LLnode * ll_pop_node(LLnode **);
void ll_destroy_node(LLnode *);

//Print functions
void print_cmd(Cmd *);

//Time functions
long timeval_usecdiff(struct timeval *, 
                      struct timeval *);

UINT8* convert_frame_to_char(Frame* frame);
Frame* convert_char_to_frame(UINT8* char_buffer);
BOOLEAN check_CRC(UINT8 msg[MAX_FRAME_SIZE]);
Frame* create_frame(tSenderID sender, tReceiverID receiver, tHeader_flag flag);

#if (USE_CRC_32 == TRUE)
tCRC_32 CRC(UINT8* array, UINT32 n_bytes);
#else
tCRC_8 CRC(UINT8* array, UINT32 n_bytes);
#endif

#endif
